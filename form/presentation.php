<?php
/**
 * Created by PhpStorm.
 * User: deval
 * Date: 20-05-19
 * Time: 22:46
 */
try{
    $db = new PDO('mysql:host=localhost;dbname=devalet;', 'devalet', '0cI6C0lH');
    $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

}
catch(PDOException $e){

}
function formPresent($db){
    $requestPresent = 'SELECT titre,texte from `te_present`';
    $reponsePresent = crudDb($db, $requestPresent);
    $linePresent = $reponsePresent->fetch();

    $requestImage='SELECT nom_dossier,nom_fichier,extension FROM `te_image` where position ="presentation" ';
    $reponseImage=crudDb($db,$requestImage);
    $lineImage=$reponseImage->fetch();
    $image = $lineImage['nom_dossier'].'/'.$lineImage['nom_fichier'].$lineImage['extension'];

    echo '
    <div class="wrapper">
    <section class="first-sec">
        <div class="left-pr">
            <h2>'.$linePresent['titre'].'</h2>
            <p class="txt-pr">'.$linePresent['texte'].'</p>
        </div>
        <div class="right-pr">
            <img class="pic-pr" src="'.$image.'">
        </div>
        </section>
        </div>
        <footer>

            <p class="footext">&copy; Copyright '.date('Y').' Cédric Devalet - Tous Droits Réservés &nbsp; Designed By Cédric Devalet</p>

        </footer>
        </body>
        </html>';
}