<?php
/**
 * Created by PhpStorm.
 * User: jona
 * Date: 28/05/19
 * Time: 15:33
 */


function getPageMenu(){

    $pages = [
        "index"=>'Home',
        "presentation"=>"About me",
        "services"=> "Services",
        "portfolio"=>"Portflio",
        "contact"=>"Contact"
    ];

    $menu = '<a href="?page=index" class="logo">ABOUT ME</a>
    <div class="menu-toggle"></div>
    <nav>
        <ul>
        ';
    foreach ($pages as $pageLink => $pageName){
        $active = '';
        if (isset($_GET['page']) && $_GET['page']===$pageLink ){
            $active= 'class="active"';
        }
        $menu .= '<li><a href="?page='.$pageLink.'" '.$active.'>'.$pageName.'</a></li>';
    }
    $menu .= '</ul>
    </nav>
    <div class="clear"></div>';

    return $menu;
}

function affichePAge($content){
    echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>presentation</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="screen.css" media="screen" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
</head>
<body>
<header>
  '.getPageMenu().'
</header>
<script src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready (function(){
        $(\'.menu-toggle\').click(function(){
            $(\'.menu-toggle\').toggleClass(\'active\')
            $(\'nav\').toggleClass(\'active\')
        })
    })
</script>';
    echo $content;

    echo '
        </div>
</section>
</div>
    
<footer>

    <p class="footext">&copy; Copyright 2019 Cédric Devalet - Tous Droits Réservés &nbsp; Designed By Cédric Devalet</p>

</footer>
</body>
</html>';
}